package microsservico_externo_bicicleta;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.UnirestException;
import kong.unirest.json.JSONObject;

class CobrancaTest {

	@Test
	public void testRealizarCobrancaIntegracao() {
		HttpResponse<JsonNode> resposta = Unirest.post("https://microsservicoexternobicicleta.herokuapp.com/cobranca")
				.header("accept", "application/json")
			    .queryString("ciclista", "cc93687a-c5ce-408e-bc27-dd7597439615")
			    .queryString("valor", "5")
			    .asJson();
		
		JSONObject json = resposta.getBody().getObject();
		Cobranca cobranca = new Cobranca(json.getString("id"), json.getInt("valor"));
		
		assertEquals("5", "" + cobranca.getValor());
	}
	
	
	@Test
	public void testMicrosservicoCobrancaNotNull() throws UnirestException{
		HttpResponse<JsonNode> resposta = Unirest.post("https://microsservicoexternobicicleta.herokuapp.com/cobranca")
				.header("accept", "application/json")
			    .queryString("ciclista", "cc93687a-c5ce-408e-bc27-dd7597439615")
			    .queryString("valor", "5")
			    .asJson();	
		
		assertNotNull(resposta.getBody());
	}
	
	@Test
	public void testMicrosservicoCobrancaStatus() throws UnirestException{
		HttpResponse<JsonNode> resposta = Unirest.post("https://microsservicoexternobicicleta.herokuapp.com/cobranca")
				.header("accept", "application/json")
			    .queryString("ciclista", "cc93687a-c5ce-408e-bc27-dd7597439615")
			    .queryString("valor", "5")
			    .asJson();	
		
		assertEquals(200, resposta.getStatus());
	}

}
