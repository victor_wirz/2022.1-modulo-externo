package microsservico_externo_bicicleta;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CartaoDeCreditoTest {
	private CartaoDeCredito cartao;
	@BeforeEach                                         
    public void setUp() throws Exception {
        cartao  = new CartaoDeCredito();
    }
	
	@Test
	public void testGetBandeira() {
		cartao.setNumero("4111111145551141");
		assertEquals("Visa", cartao.getBandeira());
	}
	
	// teste da consulta de cartoes de credito
	@Test 
	public void testExcecaoCartaoNaoEncontrado() throws CartaoNaoEncontrado{
		Exception exception = assertThrows(CartaoNaoEncontrado.class, () -> {
			CartaoDeCredito.consultarCartoes("Victor Wirz");
	    });	
	}
	
	// Testa o numero do cartao do ciclista
	@Test
	public void testConsultaCartao() throws CartaoNaoEncontrado{
		assertEquals("4111111145551141", CartaoDeCredito.consultarCartoes("Victor Hugo").getNumero());
	}
	
	@Test 
	public void testeRetornarCartoes() {
		assertEquals(3, CartaoDeCredito.retornarCartoes().size());
	}
}
